# OSCP BoF

A few things I made to help me along with the buffer overflow section of the PWK course towards achieving the OSCP certification.

These scripts follow the steps laid out by TCM in the buffer overflow section of his introduction to ethical hacking course.

(all-in-one.py helps streamline the first 3 steps, it contains all the functionality of fuzz.py, offset-finder.py and overwrite-EIP.py)
